# Installation of Grafana

## 1. connect with your personnal account

```bash
ssh <username>
```

## 2. Install Grafana and Prometheus installation script and execute it

```bash
kubectl create namespace monitoring
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install pormetheus prometheus-community/prometheus
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
helm install grafana grafana/grafana

```

## 3. Web deployment 

```bash
kubectl apply -f ingress.yaml -n monitoring
kubectl exec -it -n monitoring pod-name -- /bin/bash
grafana-cli admin reset-admin-password admin
```
## 3. Web configuration

Sur l'interface web de Grafana > Data source > Add data source
Ajouter le serveur Prometheus pour la suite 
