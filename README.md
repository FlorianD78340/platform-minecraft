# Minecraft Platform (GitLab CI/CD)

## GitLab CI/CD Configuration
Before using GitLab CI/CD with this project, you must configure the following:

### Prerequisites

- A GitLab Runner is installed on the Kubernetes cluster with the `kube` tag.
- A GitLab Runner is installed on the machine with the `build` tag.
- Generate a new repository access token with `api`, `read repository`, and `write repository` rights.
- Configure the environment variables for sensitive information (API key).

You must add the `GITLAB_TOKEN` variable in the project settings with the following options:

- **Protected**: Enabled to prevent accidental or malicious modification of the variable.
- **Masked**: Enabled to hide the variable value in logs and outputs.
- **Expanded**: Enabled to make the variable available to all stages of the pipeline.

## GitLab CI Pipeline Stages

The `.gitlab-ci.yml` file describes the various stages of the CI/CD pipeline. Here's a brief explanation of each stage:

- `lint`: Code quality check using `yamllint`.

- `build`: Build the Docker image of the service and register it to the Docker registry. This stage is executed on every commit on a branch other than `main`.

- `test`: Docker image security analysis using `Container Scanning` provided by GitLab.

- `deploy-dev`: Deploy the service to the development environment.

- `deploy-prod`: Deploy the service to the production environment. This stage is executed only on a merge to the `main` branch.

- `healthcheck`: Check the service health by executing an HTTP request on port `25565`.

- `build-release`: Build and publish the release. This stage is executed only on a merge to the `main` branch and creates a new release using `semantic-release`.

| Branche | Étapes exécutées |
| ------- | ---------------- |
| `main`  | - `lint` - `build` - `test` - `deploy-prod` - `healthcheck` - `build-release`  |
| `*`   | - `lint` - `build` - `test` - `deploy-dev` - `healthcheck`|

### Deployment to Development Environment
---

Deployment to the development environment is automatically performed on every commit to the `dev` branch. It uses `kubectl` to deploy the application to the corresponding Kubernetes cluster.

### Deployment to Production Environment
---

Deployment to the production environment is automatically performed on a merge to the `main` branch. It uses `kubectl` to deploy the application to the corresponding Kubernetes cluster.

### Release Publishing
---

Release publishing is automatically performed on a merge to the `main` branch. It uses `semantic-release` to generate a new release from the commits made since the last release. The release publishing steps include generating release notes, incrementing the version number, creating a Git tag, and publishing the new release to GitLab.