## [1.0.0](https://gitlab.com/FlorianD78340/platform-minecraft/compare/...1.0.0) (2023-05-05)


### Bug Fixes

* healthcheck dev and prod only ([3d1ea90](https://gitlab.com/FlorianD78340/platform-minecraft/commit/3d1ea90961c6386714ed556416871efce031c951))
