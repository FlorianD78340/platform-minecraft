# Deploy traefik routes 

***What is Traefik?***

Traefik is a popular open-source reverse proxy and load balancer that is designed to work seamlessly with modern containerized applications and microservices architectures.  
It integrates with container orchestration platforms like Kubernetes, Docker Swarm, and Mesos, and can automatically discover and configure routing rules for services running in these environments.  

Traefik supports a wide range of protocols and can automatically generate SSL/TLS certificates using Let's Encrypt. It also includes advanced features like circuit breakers, retries, and rate limiting to improve the reliability and resilience of your applications.  
With its simple configuration syntax and intuitive dashboard, Traefik has become a popular choice for managing traffic in cloud-native environments.

**Requirements: traefik** 

Traefik is by default in k3s

## Check the service that you want to expose

```bash
kubectl get svc -n <namespace_where_your_services_is_deployed>
```

## Write the ingress according to your service  

Here is a template of a Traefik ingress without TLS:  
```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: grafana-ingress
  annotations:
    traefik.ingress.kubernetes.io/router.entrypoints: web
spec:
  rules:
    - host: "grafana.mc.lab"
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: <service_name>
                port:
                  number: <port_of_the_service>
```
## Deploy the ingress and verify the deployment

Deploy your ingresses to Kubernetes cluster:  
```bash
kubectl apply -f <name_of_your_file> -n <namespace_name>
```
Check if your ingresses are deployed:  
```bash
kubectl get ingress -n <namespace_name>
```
## Test the access in your navigator

In case the hostname used in the ingress configuration is not configured in a external DNS server, you have to modify your hosts file.

Add to your /etc/hosts (for linux) file, this line

```bash
<ip of your loadbalancer> <hostname>
```

For windows, you can find the hosts file in C:\Windows\system32\drivers\etc\hosts:  

```bash
<ip of your loadbalancer> <hostname>
```

example:
```yaml
10.101.15.99 grafana.mc.lab
```
Now you can use the hostname in your navigator?

## Update your ingresses

To update your ingresses, you have to change the service name with the right service name of your service. Then change the port to the right port to.  
If you want to deploy an ingress with TLD contact an admin K8S.
