# "platform-minecraft" provides a platform !

Created By: Franck Brouard
Date Created: April 13, 2023 11:53 AM
Implementation Status: Done
Last Edited By: Franck Brouard
Last Updated: May 3, 2023 1:47 PM
Person: Franck Brouard
Tags: Back 2 Basics

## What is a platform ?

A platform is a product packaging tools, services, processes, collaboration and infrastructure that provides a foundation for designing, maintaining, developing and deploying applications.

## Advantages of platform engineering

Platform engineering offers several advantages: consistency, efficiency, scalability, resilience, and innovation. 

Akeneo needs platform engineering because we have complex applications and business domains that require specialized infrastructure, development tools, and deployment processes to enable efficient and reliable delivery of software products.

In terms of Production Standard, a well-designed Platform Engineering solution can enable teams to achieve a higher level of production readiness, as it provides a set of standardized tools and practices that can be shared across the organization.

### The Power of Platform Teams Leveraging Platforms

It's essential to note that even if a platform is provided to a team, that platform itself may be built using other platforms. 

At Akeneo, we use Google Cloud Platform (GCP) for compute and Google Kubernetes Engine (GKE) for containers, and then providing a complete platform on top of that with the help of a platform team, we’ll reduce the cognitive load on developers and operators. They no longer have to worry about the underlying infrastructure but can instead focus on the higher-level platform services and functionality they are responsible for. This can result in more efficient and effective development and operations, ultimately leading to a more reliable and scalable platform.

![Untitled](leveraging_platform.png)

## Stream-aligned teams at Akeneo

Platform engineering is bases on stream-aligned teams focusing on specific applications or business domain areas.

Here are some examples of what stream-aligned teams could be :

- K3S team : Focused on maintaining the platform infrastructure and services
- CICD team : Focused on plateform pipeline
- Observability team : Focused on observability tools

We could have :

- SDO Teams: Focused on designing and implementing the software delivery pipeline and associated tooling.
- SRE team: Focused on the application's infrastructure and systems' reliability, availability, and performance.
- Data team: Focused on managing, processing, and analyzing the application's data.
- Security team: Focused on ensuring the security and compliance of the application, including implementing security measures, conducting audits, and responding to security incidents.

![Untitled](stream_aligned_teams.png)

## Components of an Engineering Platform

An [internal developer platform (IDP)](https://internaldeveloperplatform.org/core-components/) is a key component of platform engineering, providing a self-service infrastructure that enables developers to focus on delivering business value rather than managing infrastructure.

As described here, IDP has 5 core components :

| Core Component | Short Description |
| --- | --- |
| https://internaldeveloperplatform.org/core-components/application-configuration-management/ | Manage application configuration in a dynamic, scalable and reliable way. |
| https://internaldeveloperplatform.org/core-components/infrastructure-orchestration/ | Orchestrate your infrastructure in a dynamic and intelligent way depending on the context. |
| https://internaldeveloperplatform.org/core-components/environment-management/ | Enable developers to create new and fully provisioned environments whenever needed. |
| https://internaldeveloperplatform.org/core-components/deployment-management/ | Implement a delivery pipeline for Continuous Delivery or even Continuous Deployment (CD). |
| https://internaldeveloperplatform.org/core-components/role-based-access-control/ | Manage who can do what in a scalable way. |

> **Internal Developer Platform + standardized processes + team topologies + collaboration with other teams = platform engineering.**
> 

## The interfaces of the platform

To provide “on-demand” services, a platform team needs to consider what developers who use the platform need. 

Also, developers need clear interfaces to interact with the platform. This includes documentation on how to use the platform and tools and resources for troubleshooting any issues that may arise.

Like Akeneo products, it is preferable to create interfaces with input from both the platform team and devs to ensure they meet the needs of both parties (a user-centric design approach is preferred).

## platform-minecraft…

Platform-minecraft is a realization of platform engineering because it involves creating a set of reusable services and tools that multiple squads could leverage. 

UCS & Platform engineering involves several vital services that are essential for the smooth functioning of the platform but are voluntarily hidden from developers. These services include disaster recovery planning (DRP), security management, performance monitoring, and capacity planning (disposable instances)... 

Platform engineering enables developers to focus on building and delivering high-quality software by taking care of these activities behind the scenes.

By doing so, we want to reduce the cognitive load on developers, improve delivery speed, and ensure a high production standard.
